"""
Largest Prime factor

The prime factors of 13195 are 5, 7, 13, and 29.

What is the largest prime factor of the number 600851475143?
"""
__author__ = 'Charles'



def primefactors(n):
    factors = []
    x = 3
    while n >= x:
        if not n % x:
            #print(x)
            factors.append(x)
            n = n/x
        x += 2

    return factors

print(primefactors(600851475143))