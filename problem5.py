
"""
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

Factor and try with lists!!
A = reduce(lambda x,y: x+y,l) B = sum(l, []) C = [item for sublist in l for item in sublist]
"""
__author__ = 'Charles'


def smallest_multiple(y, z):
    """ Find the smallest multiple.
    Args:
        x: the min number
        y: the max number
    Returns:
        smallnum
    Raises:
        TypeError:
        ValueError:
    """

    #Pre Condition
    assert (y == int(y) and y >= 0), "This is not a natural number"
    assert (z == int(z) and z >= 0), "This is not a natural number"
    smallnum = 0
    cap = 1
    count = z

    for x in range(z-7, z):
        cap *= z

    while count < cap:
        for x in range(y, z):
            #print(count)
            if count % x:
                count += z
                break
            elif x == z-1:
                return count

    #Post Condition
    assert (count > 0), "The variable 'total' is less than zero: " + str(count)
    return smallnum


def sometests():
    assert (2520 == smallest_multiple(1, 10)), "The Default case is not working"
    print(smallest_multiple(1, 20))
    #print(sum_multiples(10.22))
    #print(sum_multiples(-10))

sometests()
