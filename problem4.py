__author__ = 'Charles'
"""
A palindromic number reads the same both ways. The largest palindrome
from the product of two 2-digit numbers is 9009 = 91 x 99

Find the largest palindrome made from the product of two 3-digit numbers
"""


def palindromeproduct(digitlow, digithigh):

    palindromedic = {}
    for x in range(digithigh, digitlow, -1):
        for y in range(digithigh, digitlow, -1):
            sumofproduct = x * y
            #print(sumofproduct)
            tempstring = str(sumofproduct) + str(sumofproduct)[::-1]
            if not (int(tempstring) % sumofproduct):
                palindromedic[sumofproduct] = x, y
    return palindromedic

tempa = palindromeproduct(100, 999)
tempb = max(tempa)

print(tempb, tempa.get(tempb))


"""
test = 1234

print(str(test)[::-1])

temp = str(test)+str(test)[::-1]
print(temp)
"""