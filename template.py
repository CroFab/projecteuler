"""
List program information
"""
__author__ = 'Charles'


def sum_multiples(y):
    """ Calculate the sum of multiples.
    Args:
    Returns:
    Raises:
        TypeError:
        ValueError:
    """

    #Pre Condition
    assert (y == int(y) and y >= 0), "This is not a natural number"
    total = 0

    for x in range(1, y):
        if not (x % 3) or not (x % 5):
            total += x

    #Post Condition
    assert (total > 0), "The variable 'total' is less than zero: " + str(total)
    return total


def main():
    assert (23 == sum_multiples(10)), "The Default case is not working"
    print(sum_multiples(1000))
    #print(sum_multiples(10.22))
    #print(sum_multiples(-10)
    #need a new test case

if __name__ == "__main__":
    main()

